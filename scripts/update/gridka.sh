#!/usr/bin/env bash
#
# Create / remove DNS records from RocksDB
#
# Intended as an update script to the KIT NetDB Smart Proxy plugin [1].
#
# Author: Joshua Bachmeier <joshua.bachmeier@student.kit.edu>
#
# Recognized environment variables:
# - PROXY_GRIDKA_MAIL_RECV
#     If set, send notification to this mail address if the
#     requested IP address is not in the 10.0.0.0/8 subnet
# - PROXY_GRIDKA_SMTP_SERVER
#     The SMTP relay server to use for sending mail. If unset,
#     use no special SMTP server

#BASE="https://10.97.1.253/install/sbin/public"
BASE="https://jupiter.gridka.de/install/sbin/public"
ENDPOINT="$BASE/configHost_fromForeman.cgi"
CURLFLAGS="--fail --insecure"

mycurl() {
    curl $CURLFLAGS $ENDPOINT$@ >&2
}

action=$1
! shift && echo "No action provided" >&2 && exit 64

case $action in
    --create)
        fqdn=$1
        ! shift && echo "No FQDN provided" >&2 && exit 64
        ipaddr=$1
        ! shift && echo "No IPADDR provided" >&2 && exit 64
        type=$1
        ! shift && echo "No TYPE provided" >&2 && exit 64

        [[ ! $fqdn =~ '.gridka.de.'$ ]] && echo "Unsupported domain $fqdn (must be subdomain of \`gridka.de')" >&2 && exit 65
        [[ $type != "A" ]] && echo "Unsupported record type $type (must be \`A')" >&2 && exit 65

        name=${fqdn%.gridka.de.}

        echo "Creating $type entry $fqdn -> $ipaddr" >&2
        mycurl "?cmd=add&hostname=${name}&ip=${ipaddr}"
    ;;
    --remove)
        fqdn=$1
        ! shift && echo "No FQDN provided" >&2 && exit 64
        type=$1
        ! shift && echo "No TYPE provided" >&2 && exit 64

        [[ ! $fqdn =~ '.gridka.de.'$ ]] && echo "Unsupported domain $fqdn (must be subdomain of \`gridka.de')" >&2 && exit 65
        [[ $type != "A" ]] && echo "Unsupported record type $type" >&2 && exit 65

        name=${fqdn%.gridka.de.}

        echo "Removing $type entry $fqdn" >&2
        mycurl "?cmd=remove&hostname=${name}"
    ;;
    --suggest)
        subnet_base=${1%/*}
        ! shift && echo "No SUBNET BASE provided" >&2 && exit 64

        ENDPOINT="$BASE/getFreeIP.cgi"
        mycurl "?subnet=${subnet_base}" \
            | sed 's/^/+ /'
    ;;
    *)
        echo "Invalid command \`$action\`" >&2
        exit 64
        ;;
esac
