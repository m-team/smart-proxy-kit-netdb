#!/usr/bin/env bash

action=$1
shift
case $action in
    --create)
        fqdn=$1
        ipaddr=$2
        type=$3

        echo "Creating $type entry $fqdn -> $ipaddr"

        echo "$ipaddr" > "/tmp/${type}.${fqdn}record"
    ;;
    --remove)
        fqdn=$1
        type=$2

        echo "Removing $type entry $fqdn"
        rm "/tmp/${type}.${fqdn}record"
        ;;
    --suggest)
        subnet=$1
        echo "+ $subnet"
        find /tmp -name '*.*.record' -exec cat {} \; | sed 's/^/- /'
        ;;
    *)
        echo "Unknown command $action"
        exit 1
esac
