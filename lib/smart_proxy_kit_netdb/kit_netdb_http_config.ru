require 'smart_proxy_kit_netdb/kit_netdb_api'

map '/netdb' do
  run Proxy::KIT::NetDB::Api
end
