require 'smart_proxy_kit_netdb/kit_netdb_main'
require 'ip'
require 'uri'

module Proxy
  module KIT
    module NetDB
      module Validators
        class AtMostOnePresent < ::Proxy::PluginValidators::Base
          def validate!(settings)
            names = [@setting_name] + @params

            num_present = names.count {|name| !settings[name].nil?}
            if num_present > 1
              raise ::Proxy::Error::ConfigurationError, "At most one must pe bresent of #{names}"
            end

            true

          end
        end

        class ValidSubnet < ::Proxy::PluginValidators::Base
          def validate!(settings)
            if !settings.has_key? @setting_name
              settings[@setting_name] = {:subnets => [], :domains => []}
              return
            end

            if settings[@setting_name][:subnets].nil?
              settings[@setting_name][:subnets] = []
              settings[@setting_name][:domains] = []
            elsif !settings[@setting_name][:subnets].is_a? Array
              settings[@setting_name][:subnets] = [settings[@setting_name][:subnets]]
            end

            settings[@setting_name][:subnets].each do |subnet|
              begin subnet == :unknown or IP.new(subnet)
              rescue ArgumentError
                raise ::Proxy::Error::ConfigurationError, "Invalid subnet specification: #{subnet}"
              end
            end

          end
        end

      end

      class Plugin < ::Proxy::Plugin
        plugin 'netdb', ::Proxy::KIT::NetDB::VERSION

        default_settings tls: true, netdb_server: 'https://www-net.scc.kit.edu',
                         # DNS options
                         :dns_only_for_netdb_subnets => false,
                         :perform_nsupdate => false,
                         :netdb => {:reserved_first_ips => 0},
                         :dns_server => 'localhost'

        load_validators at_most_one_present: Validators::AtMostOnePresent,
                        valid_subnet: Validators::ValidSubnet

        validate :dont_update_per_netdb, valid_subnet: true
        validate :update_per_mail, valid_subnet: true
        validate :update_per_script, valid_subnet: true

        validate :client_cert_path, at_most_one_present: [:client_p12_path, :client_cert_gpg]
        validate_readable :client_cert_path, :client_cert_key_path, :client_p12_path

        validate :client_cert_path, presence: true,
                 if: lambda {|settings| !settings[:client_cert_key_path].nil?}

        validate :dns_key, file_readable: true,
                 if: lambda {|settings| settings[:perform_nsupdate]}

        http_rackup_path File.expand_path('kit_netdb_http_config.ru', File.expand_path('../', __FILE__))
        https_rackup_path File.expand_path('kit_netdb_http_config.ru', File.expand_path('../', __FILE__))

        load_classes ::Proxy::KIT::NetDB::PluginConfiguration
        load_dependency_injection_wirings ::Proxy::KIT::NetDB::PluginConfiguration

      end
    end
  end
end
