require 'smart_proxy_kit_netdb/kit_netdb_dns_per_script'
require 'json'

module Proxy
  module KIT
    module NetDB
      class Provider
        include ::Proxy::Log


        def initialize settings
          @script_backend = DNS::ScriptBackedRecord.new settings
          @netdb_backend = DNS::NetDBBackedRecord.new settings
          load_allocated_ips
        end

        def unmapped_ip subnet: nil, mask: nil, from: nil, to: nil, mac: nil
          raise ArgumentError, "missing keyword: subnet" if subnet.nil?

          if mask.nil?
            subnet = IP.new(subnet)
          else
            cidr = IP.new(mask).to_i.to_s(2).count("1")
            if subnet.include? "/"
              if cidr != subnet.split("/")[1].to_i
                raise ArgumentError, "Cidr spec #{subnet} does not match netmask #{mask} (expected /#{cidr})"
              else
                subnet = IP.new(subnet)
              end
            else
              subnet = IP.new("#{subnet}/#{cidr}")
            end
          end

          from = IP.new(from) unless from.nil?
          to = IP.new(to) unless from.nil?

          subnet_address = subnet.to_addr
          subnet_address = netdb_backend.defer_exact_value subnet_address

          range = validate_range from, to, subnet
          occupied_ips = []

          if script_backend.enabled_for? nil, subnet_address
            free_ips, occupied_ips = script_backend.get_free subnet
            range = free_ips.select {|snet| range.first <= snet.to_range.first && range.last >= snet.to_range.last}
            range = range.flat_map {|snet| snet.to_range.to_a}
          end

          if netdb_backend.enabled_for? nil, subnet_address
            occupied_ips = netdb_backend.get_occupied subnet
          end

          find_free_ip range, occupied_ips
        end

        private

        def find_free_ip range, occupied_ips
          clear_expired_ips!

          pool = range
                   .reject {|ip| occupied_ips.include?(ip)}
                   .reject {|ip| allocated_ips.include?(ip)}

          free_ip = pool.to_a.sample
          raise RuntimeError, "No free IP address" unless free_ip
          allocate_ip free_ip

          save_allocated_ips

          free_ip
        end

        def validate_range from, to, subnet
          r = subnet.to_range
          Range.new([r.min, from].compact.max,
                    [r.max, to].compact.min)
        end

        def clear_expired_ips!
          now = Time.now

          # Delete everything older than an hour
          allocated_ips.reject! {|_, time| time + (60*60) < now}
        end

        def allocate_ip ip
          time = Time.now
          logger.info "Reserving IP address #{ip} until #{time + 60*60}"
          allocated_ips[ip] = time
        end

        def save_allocated_ips
          File.open("/tmp/smart-proxy-kit-netdb-allocated-ips", "w") do |file|
            file.write(allocated_ips.to_json)
          end
        end

        def load_allocated_ips
          @allocated_ips = {}

          if File.exists?("/tmp/smart-proxy-kit-netdb-allocated-ips")
            raw_allocated_ips = JSON.parse(
              File.read("/tmp/smart-proxy-kit-netdb-allocated-ips")
            )

            raw_allocated_ips.each_pair do |ip, time|
              @allocated_ips[IP.new(ip)] = Time.parse(time)
            end
          end
        end

        attr_reader :allocated_ips, :script_backend, :netdb_backend
      end
    end
  end
end
