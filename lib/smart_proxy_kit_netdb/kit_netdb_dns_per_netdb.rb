# coding: utf-8
require 'smart_proxy_kit_netdb/utils'
require 'kit/netdb'

module Proxy
  module KIT
    module NetDB
      module DNS
        class NetDBBackedRecord < ::Proxy::Dns::Record
          include ::Proxy::Log
          include ::Proxy::Util

          attr_reader :api

          def initialize settings
            super('localhost')

            settings = {:dont_update_per_netdb => { :domains => [], :subnets => []},
                        :netdb => {:reserved_first_ips => 0 }}.merge settings

            @netdb_server = settings[:netdb_server]
            @args = {}

            if settings[:tls]
              @args[:use_ssl] = true

              if settings[:client_p12_path]
                pkcs12 = OpenSSL::PKCS12.new File.read(settings[:client_p12_path]), ""
                @args[:cert] = pkcs12.certificate
                @args[:key] = pkcs12.key

              elsif settings[:client_cert_path] || settings[:client_cert_gpg]
                @args[:cert] = OpenSSL::X509::Certificate.new(
                  if settings[:client_cert_path]
                    File.read settings[:client_cert_path]
                  elsif settings[:client_cert_gpg]
                    `gpgsm --export #{settings[:client_cert_gpg]}`
                  end)

                @args[:key] = OpenSSL::PKey.read(
                  if settings[:client_cert_key_path]
                    File.read settings[:client_cert_key_path]
                  elsif settings[:client_cert_path]
                    File.read settings[:client_cert_path]
                  elsif settings[:client_cert_gpg]
                    `gpgsm --export-secret-key-raw #{settings[:client_cert_gpg]}`
                  else
                    raise ArgumentError, "Found client certificate, but no key"
                  end)
              end

            end

            @only_for_netdb_subnets = settings[:dns_only_for_netdb_subnets]
            @excluded_domains = DomainList.new settings[:dont_update_per_netdb][:domains]
            @excluded_subnets = SubnetList.new settings[:dont_update_per_netdb][:subnets]

            @num_reserved_ips = settings[:netdb][:reserved_first_ips]
          end

          def do_create(fqdn, value, type)
            fqdn += '.' unless fqdn.end_with? '.'

            logger.debug "Connecting to NetDB WebAPI"
            connected do
              api.default.dns.do {|dns|

                record_type = rank_inttypes(dns, type).first
                logger.debug "Using DNSVS inttype #{record_type[:name]} for record type #{type}"

                logger.info "Creating DNSVS record #{fqdn} -> #{value.get}"
                begin
                  dns.record.create new_data: value.get,
                                    new_fqdn: fqdn,
                                    new_inttype: record_type[:name]

                rescue ::KIT::NetDB::FunctionalError => e
                  # This only catches "[dns] RR schon vorhanden", so only if both fqdn and value match.
                  # If there were an existing entry for the given fqdn and a different value, "23P01" i.e.
                  # "[dns] RR-Satz darf nur aus genau einem RR bestehen" would have been returned.
                  if e.error_code == "23505"
                    logger.warning "Record for #{fqdn} -> #{value.get} already exists (#{e})"
                  else
                    raise e
                  end
                end

                nil
              }
            end
            nil
          end

          def do_remove(fqdn, type)
            fqdn += '.' unless fqdn.end_with? '.'

            logger.debug "Connecting to NetDB WebAPI"
            connected do
              api.default.dns.do {|dns|

                # The delete WAPI function only provides a inttype filter, not a record type filter,
                # so we first need to find the inttypes for the given record type ...
                logger.debug "Searching applicable DNSVS inttype for record type #{type}"
                inttypes = dns.record_inttype
                             .list(record_type: type)

                inttypes.select! {|r| r[:owner_fqdn_inttype].split(':')[1][1] == '1'} if ["A", "AAAA"].include? type

                inttypes.map! {|r| r[:name]}

                # Get the record data, which is required for deletion
                logger.debug "Fetching to-be-deleted DNSVS records"
                records = dns.record
                            .list(fqdn: fqdn,
                                  inttype: inttypes)

                if records.empty?
                  logger.warning "No records of type #{type} for #{fqdn}"
                  return nil
                end

                records.each do |r|
                  logger.info "Deleting DNSVS record #{r[:fqdn]} -> #{r[:data]} (#{r[:inttype]}) "
                  dns.record.delete old_fqdn: r[:fqdn],
                                    old_inttype: r[:inttype],
                                    old_data: r[:data]
                end
              }
            end

            nil
          end

          def value_of fqdn, type
            connected do
              api.default.dns.do do |dns|

                records = dns.record.list fqdn: fqdn, type: type

                if records.empty?
                  logger.info "NetDB doesn't have an entry of type #{type} for FQDN #{fqdn}"
                  :unknown
                else
                  logger.info "Found #{type} value #{records} in NetDB for FQDN #{fqdn}"
                  records[0][:data]
                end
              end
            end
          end

          def enabled_for? fqdn, value, type=nil
            if [nil, "A", "AAAA"].include? type
              # "only_for_netdb_subnets => subnet_enabled_by_foreman"
              foreman_enabled = @only_for_netdb_subnets \
                                ? subnet_enabled_by_foreman?(value, type) \
                                : true
              config_disabled = (fqdn == nil || @excluded_domains.include?(fqdn)) \
                                && @excluded_subnets.include?(value.get)

              foreman_enabled && !config_disabled
            else
              true
            end
          end

          def get_occupied subnet
            connected do
              subnet = get_netdb_subnet subnet
              raise ArgumentError, "subnet not found or no access: #{subnet}" unless subnet
              logger.debug "Using subnet #{subnet}"

              all_occupied_ips(subnet[:name]) + subnet[:ip].to_range.take(@num_reserved_ips + 1).to_a
            end
          end

          private

          def rank_inttypes dns, type
            logger.debug "Searching applicable DNSVS inttypes for record type #{type}"

            # We first need to find a suitable inttype.
            # We require it to be of the corect record_type (Duh)
            record_types = dns.record_inttype.list(record_type: type)

            # Require it to be hostname capabel, if A or AAAA
            record_types.select! {|r| r[:owner_fqdn_inttype].split(':')[1][1] == '1'} if ["A", "AAAA"].include? type

            log_halt 404, "No applicable DNSVS inttype found for #{type}" if record_types.empty?

            # Sort by appropriateness
            record_types
              .sort {|r, v| record_inttype_sortkey(r) <=> record_inttype_sortkey(v)}
          end

          def record_inttype_sortkey r
            [r[:owner_fqdn_inttype].split(':')[1][0],            # Terminal FQDN < Non-terminal
             SortDescending.new(r[:target_uniqueness][:value]),  # The more unique, the better
             r[:name]]                                           # For determinism
          end

          # See: https://stackoverflow.com/a/15440336
          class SortDescending < Struct.new(:value)
            def <=>(other)
              other.value <=> value
            end
          end

          def subnet_enabled_by_foreman? value, type
            ip = IP.new(value.get)

            logger.debug "Verifying with Foreman that #{ip} is in a NetDB managed subnet"

            http = ::Proxy::HttpRequest::ForemanRequest.new
            req = http.request_factory
                    .create_get('api/netdb/subnets')

            req['Accept'] = 'application/json'  # TODO This should be done by RequestFactory#create_get

            rsp = http.send_request(req)
            rsp.value # Raise error if not OK
            rsp = JSON.parse rsp.read_body

            subnet = rsp["results"]
                       .map {|s| s["subnet"]}
                       .map {|s| s.merge(
                               "cidr" => IP.new(s["mask"]).to_i.to_s(2).count("1")
                             )}
                       .find {|s| IP.new("#{s["network"]}/#{s["cidr"]}").network.to_range.include? ip}

            if subnet.nil?
              logger.info "No NetDB enabled subnet contains #{ip}. Skipping."
              false
            else
              logger.debug "Subnet #{subnet["network"]}/#{subnet["cidr"]} enabled for NetDB. Proceeding."
              true
            end
          end

          def connected
            ::KIT::NetDB::API.connect(@netdb_server, **@args) do |connection|
              @api = connection.api
              yield
            end
          ensure
            @api = nil
          end

          def get_netdb_subnet subnet
            range = subnet.to_range
            api.default.dns.range.list
              .map {|range| {
                      :ip => IP.new(range[:ip_subnet_cidr_mask] ||
                                    range[:subnet_cidr_spec]),
                      :name => range[:name]
                    }}
              .select {|subnet| subnet[:ip].to_range.first <= range.first && subnet[:ip].to_range.last >= range.last}
              .first
          end

          def all_occupied_ips subnet_name
            api.default.dns.record
              .do {list(range: subnet_name, type: "A") \
                   + list(range: subnet_name, type: "AAAA")}
              .map {|record| IP.new(record[:data])}
          end

          public

          # Get the value of the given FQDN from NetVS, but be lazy about it
          def defered_value_of fqdn, type
            ValueCache.new self, fqdn, type
          end

          # Wrap the given VALUE in a dummy-object that can be used
          # just like the result of #defered_value_of
          def defer_exact_value value
            DummyValueCache.new value
          end

          class ValueCache
            def initialize netdb_backend, fqdn, type
              @backend = netdb_backend

              @fqdn = fqdn
              @type = type
              @value = nil
            end

            # Return the value. If not done yet, retrive the value from NetVS
            def get
              @value ||= @backend.value_of @fqdn, @type
            end
          end

          class DummyValueCache
            def initialize value
              @value = value
            end

            def get
              @value
            end
          end

        end
      end
    end
  end
end
