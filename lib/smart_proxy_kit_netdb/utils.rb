require 'ip'

module Proxy
  module KIT
    module NetDB
      module DNS
        class DomainList
          def initialize domains
            domains = [] unless domains
            domains = [domains] unless domains.is_a? Array
            domains = Array.new domains

            @include_unknown = !domains.delete(:unknown).nil?
            @domains = domains.map {|domain| domain.end_with?('.') ? domain : domain + '.'}
          end

          def include? fqdn
            if fqdn == :unknown
              return @include_unknown
            end

            fqdn += '.' unless fqdn.end_with? '.'

            @domains.any? {|domain| fqdn.end_with? domain}
          end

          def to_a
            @domains
          end
        end

        class SubnetList
          def initialize subnets
            subnets = [] unless subnets
            subnets = [subnets] unless subnets.is_a? Array
            subnets = Array.new subnets

            @include_unknown = !subnets.reject! {|ip| ip.is_a?(Symbol) && ip == :unknown}.nil?
            @subnets = subnets.map {|subnet| IP.new(subnet).network}
          end

          def include? ip
            if ip.is_a?(Symbol) && ip == :unknown
              return @include_unknown
            end

            ip = IP.new(ip) unless ip.is_a? IP

            @subnets.any? {|subnet| (ip.to_i & subnet.netmask.to_i) == subnet.to_i}
          end

          def to_a
            @subnets
          end
        end
      end
    end
  end
end
