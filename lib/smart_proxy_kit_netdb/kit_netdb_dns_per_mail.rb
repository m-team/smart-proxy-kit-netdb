# coding: utf-8
require 'smart_proxy_kit_netdb/utils'
require 'net/smtp'

module Proxy
  module KIT
    module NetDB
      module DNS
        class MailBackedRecord < ::Proxy::Dns::Record
          include ::Proxy::Log
          include ::Proxy::Util

          def initialize settings
            super('localhost')

            settings.default= {}
            settings = { :domains => [], :subnets => []}.merge settings[:update_per_mail]

            @domains = DomainList.new settings[:domains]
            @subnets = SubnetList.new settings[:subnets]

            @mailto = settings[:mailto]
            @msg = settings[:msg] || <<EOF
However, the server was (partially) unable to fullfill the request.
An entry to the NetVS has been made, if that was requested.
EOF
            @contact = settings[:contact]

            tuple = (settings[:smtp_proxy] || 'localhost').split ':'
            if tuple.length > 1
              @smtp_host = tuple[0..-2].join(':')
              @smtp_port = tuple[-1]
            else
              @smtp_host = tuple[0]
              @smtp_port = nil
            end
          end

          def with_smtp_connection
            Net::SMTP.start(@smtp_host, @smtp_port) do |smtp| yield smtp end
          end

          def do_create fqdn, value, type
            fqdn += '.' unless fqdn.end_with? '.'

            logger.debug "Initialising SMTP connection"
            with_smtp_connection do |smtp|
              msg = compose_mail :create, fqdn, value.get, type
              logger.debug "Sending Mail to #{@mailto} via #{@smtp_host}"
              smtp.send_message msg, "noreply@#{`hostname -f`.strip}", @mailto
            end

            nil
          end

          def do_remove fqdn, type
            fqdn += '.' unless fqdn.end_with? '.'

            with_smtp_connection do |smtp|
              msg = compose_mail :remove, fqdn, type
              logger.debug "Sending Mail to #{@mailto} via #{@smtp_host}"
              smtp.send_message msg, "noreply@#{`hostname -f`.strip}", @mailto
            end

            nil
          end

          def enabled_for? fqdn, value, type
            @mailto && (!["A", "AAAA"].include?(type) || @domains.include?(fqdn) && @subnets.include?(value.get))
          end

          def compose_mail mode, fqdn, value=nil, type
            hostname = `hostname -f`.strip

            if @contact
              contact_form = "If you have any issues, please contact #{@contact}.\n"
            end

            <<EOF
From: Foreman Proxy <noreply@#{hostname}>
To: #{@mailto}
Subject: [foreman-dns-proxy] #{mode.capitalize} DNS Record for #{fqdn}

Hello!

The Smart Proxy at #{hostname} recieved a request to #{mode} a new DNS record:
> TYPE: #{type}
> FQDN: #{fqdn}
#{value ? "> VALUE: #{value}\n" : ""}
#{@msg}

Please #{mode} the DNS record as specified above.


Thank you!

#{contact_form}

--
This E-mail was automatically generated because the following conditions arose:
1. The FQDN is a subdomain of one of the following domains:
#{@domains.to_a.map {|elt| "- #{elt}"}.join("\n")}

2. The target IP address is in one of the following subnets:
#{@subnets.to_a.map {|elt| "- #{elt}"}.join("\n")}
EOF
          end

        end
      end
    end
  end
end
