require 'sinatra'
require 'smart_proxy_kit_netdb/kit_netdb'
require 'smart_proxy_kit_netdb/kit_netdb_main'

module Proxy
  module KIT
    module NetDB

      class Api < ::Sinatra::Base
        include ::Proxy::Log
        helpers ::Proxy::Helpers
        authorize_with_trusted_hosts
        authorize_with_ssl_client
        use Rack::MethodOverride

        def self.container_instance
          @container_instance ||= ::Proxy::Plugins.instance.find {|p| p[:name] == :netdb }[:di_container]
        end
        extend ::Proxy::DependencyInjection::Accessors

        inject_attr :netdb_provider, :net

        get '/dns/unmapped_ip' do
          begin
            status = 200
            headers = {}
            params.delete("captures")
            data = {"ip" => net.unmapped_ip(**Hash[params.map {|k,v| [k.to_sym, v]}])}

            [status, headers, [data.to_json]]
          rescue => e
            log_halt 500, e
          end
        end
      end
    end
  end
end
