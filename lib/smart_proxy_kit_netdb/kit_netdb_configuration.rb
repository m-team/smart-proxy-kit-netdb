require 'dns_common/dns_common'
require 'dns_nsupdate/dns_nsupdate_main'
require 'smart_proxy_kit_netdb/kit_netdb_dns_record'
require 'smart_proxy_kit_netdb/kit_netdb_provider'

module Proxy
  module KIT
    module NetDB
      class PluginConfiguration
        def load_classes
          require 'dns_common/dns_common'
          require 'dns_nsupdate/dns_nsupdate_main'
          require 'smart_proxy_kit_netdb/kit_netdb_main'
          require 'smart_proxy_kit_netdb/kit_netdb_dns_record'
          require 'smart_proxy_kit_netdb/kit_netdb_provider'
        end

        def load_dependency_injection_wirings(container_instance, settings)

          container_instance.dependency :dns_provider, lambda {
            DNS::Record.new settings,
                            ::Proxy::Dns::Nsupdate::Record.new(
                              settings[:dns_server],
                              settings[:dns_ttl],
                              settings[:dns_key])
          }

          container_instance.dependency :netdb_provider, lambda {
            Provider.new(
              settings
            )
          }
        end
      end
    end
  end
end
