# coding: utf-8
require 'dns_common/dns_common'
require 'smart_proxy_kit_netdb/kit_netdb_dns_per_mail'
require 'smart_proxy_kit_netdb/kit_netdb_dns_per_netdb'
require 'smart_proxy_kit_netdb/kit_netdb_dns_per_script'
require 'open3'

module Proxy
  module KIT
    module NetDB
      module DNS
        class Record < ::Proxy::Dns::Record
          include ::Proxy::Log
          include ::Proxy::Util

          def initialize settings, nsupdate=nil
            super('localhost')

            @netdb_backend = NetDBBackedRecord.new settings
            @mail_backend = MailBackedRecord.new settings
            @script_backend = ScriptBackedRecord.new settings
            @nsupdate_backend = nsupdate if settings[:perform_nsupdate]
          end

          def do_create(fqdn, value, type)
            fqdn += '.' unless fqdn.end_with? '.'

            value = netdb_backend.defer_exact_value value

            if netdb_backend.enabled_for? fqdn, value, type
              netdb_backend.do_create fqdn, value, type
            end

            if script_backend.enabled_for? fqdn, value, type
              script_backend.do_create fqdn, value, type
            end

            if mail_backend.enabled_for? fqdn, value, type
              mail_backend.do_create fqdn, value, type
            end

            if nsupdate_backend
              logger.debug "Delegating to nsupdate"
              nsupdate_backend.do_create(fqdn, value, type)
            end

            nil
          end

          def do_remove(fqdn, type)
            fqdn += '.' unless fqdn.end_with? '.'

            value = netdb_backend.defered_value_of fqdn, type

            if script_backend.enabled_for? fqdn, value, type
              script_backend.do_remove fqdn, type
            end

            if netdb_backend.enabled_for? fqdn, value, type
              value.get # We need to get the value here because we will promptly remove it
              netdb_backend.do_remove fqdn, type
            end

            if mail_backend.enabled_for? fqdn, value, type
              mail_backend.do_remove fqdn, type
            end

            if nsupdate_backend
              logger.debug "Delegating to nsupdate"
              nsupdate_backend.do_remove fqdn, type
            end

            nil
          end


          # Since 47732b "Fixes #17906 - Move {create,remove}_*_record to dns_common"
          # is not included in foreman-proxy 1.14.3, we need to do this manually

          def create_a_record(fqdn, ip)
            case a_record_conflicts(fqdn, ip) #returns -1, 0, 1
            when 1 then
              raise(Proxy::Dns::Collision, "'#{fqdn} 'is already in use")
            when 0 then
              return nil
            else
              do_create(fqdn, ip, "A")
            end
          end

          def create_aaaa_record(fqdn, ip)
            case aaaa_record_conflicts(fqdn, ip) #returns -1, 0, 1
            when 1 then
              raise(Proxy::Dns::Collision, "'#{fqdn} 'is already in use")
            when 0 then
              return nil
            else
              do_create(fqdn, ip, "AAAA")
            end
          end

          def create_cname_record(fqdn, target)
            case cname_record_conflicts(fqdn, target) #returns -1, 0, 1
            when 1 then
              raise(Proxy::Dns::Collision, "'#{fqdn} 'is already in use")
            when 0 then
              return nil
            else
              do_create(fqdn, target, "CNAME")
            end
          end

          def create_ptr_record(fqdn, ptr)
            case ptr_record_conflicts(fqdn, ptr) #returns -1, 0, 1
            when 1 then
              raise(Proxy::Dns::Collision, "'#{ptr}' is already in use")
            when 0
              return nil
            else
              do_create(ptr, fqdn, "PTR")
            end
          end

          def remove_a_record(fqdn)
            do_remove(fqdn, "A")
          end

          def remove_aaaa_record(fqdn)
            do_remove(fqdn, "AAAA")
          end

          def remove_cname_record(fqdn)
            do_remove(fqdn, "CNAME")
          end

          def remove_ptr_record(name)
            do_remove(name, "PTR")
          end

          private
          attr_reader :nsupdate_backend
          attr_reader :mail_backend
          attr_reader :netdb_backend
          attr_reader :script_backend

        end
      end
    end
  end
end
