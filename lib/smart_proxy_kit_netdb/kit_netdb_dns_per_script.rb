# coding: utf-8
require 'dns_common/dns_common'
require 'smart_proxy_kit_netdb/utils'
require 'open3'

module Proxy
  module KIT
    module NetDB
      module DNS
        class ScriptBackedRecord < ::Proxy::Dns::Record
          include ::Proxy::Log
          include ::Proxy::Util

          def initialize settings
            super('localhost')

            settings.default= {}
            settings = { :domains => [], :subnets => []}.merge settings[:update_per_script]

            @domains = DomainList.new settings[:domains]
            @subnets = SubnetList.new settings[:subnets]

            @update_script = settings[:script]
            @script_env = settings[:env] || {}
          end

          def do_create(fqdn, value, type)
            fqdn += '.' unless fqdn.end_with? '.'

            run_update_script :create, fqdn, value.get, type

            nil
          end

          def do_remove(fqdn, type)
            fqdn += '.' unless fqdn.end_with? '.'

            run_update_script :remove, fqdn, type

            nil
          end

          def get_free(subnet)
            subnet = IP.new(subnet) unless subnet.is_a? IP

            pos, neg = run_update_script(:suggest, subnet)
              .map {|ip_spec| ip_spec.strip.split(' ')}
              .partition {|mod, _| mod == '+'}

            pos = pos.map {|_, ip| IP.new(ip)}
            neg = neg.map {|mod, ip| (mod == '-') ? IP.new(ip) : (raise ArgumentError, 'Invalid prefix {}', mod)}
            [pos, neg]
          end

          def run_update_script what, *args
            cmd = [@update_script, "--#{what}"] + args
            logger.info "Running #{cmd.join(" ")}"

            lines = []
            Open3.popen3(@script_env, cmd.join(" ")) do |_, stdout, stderr, thread|

              err_msg = "<no error message provided>"

              stdout.each_line {|line| lines << line.chomp}
              stderr.each_line do |line|
                err_msg = line.chomp
                logger.info "#{@update_script}: #{err_msg}"
              end

              if thread.value != 0
                raise "Record update script failed: #{err_msg}"
              end
            end

            lines
          end

          def enabled_for? fqdn, value, type=nil
            @update_script && (
              !([nil, "A", "AAAA"].include?(type)) \
              || (fqdn == nil || @domains.include?(fqdn)) && @subnets.include?(value.get)
            )
          end

        end
      end
    end
  end
end
