# coding: utf-8
require 'test_helper'
require 'rack/test'

require 'smart_proxy_for_testing'
require 'smart_proxy_kit_netdb/kit_netdb'
require 'smart_proxy_kit_netdb/kit_netdb_api'

module KIT
  class NetDBPerNetDBTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def backend **kwargs
      Proxy::KIT::NetDB::DNS::NetDBBackedRecord.send :public,
                                                       :connected, :get_netdb_subnet, :all_occupied_ips, :subnet_enabled_by_foreman?, :rank_inttypes

      Proxy::KIT::NetDB::DNS::NetDBBackedRecord.new({tls: false, netdb_server: "http://example.org"}.merge(kwargs))
    end

    def set_up
      stub_request(:get, "http://example.org/api/netdb/subnets")
        .to_return(body: { "results": [{ "subnet": {
                                                     "mask": "255.255.255.128",
                                                    "network": "42.42.42.0",
                                                   }}]}.to_json)

      stub_request(:get, "http://example.org/api/2.1/dns/record_inttype/list?record_type=A")
        .to_return(body: [{"target_uniqueness": {"value": 0, "description": "RR-Sätze erlaubt; keine Zieleindeutigkeit verlangt"}, "description": "A-RR-Set für Host", "owner_fqdn_auto_dml": {"value": 1, "description": "Automatische FQDN-Behandlung über eine Ebene"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:0100", "name": "dflt:0100,:,400,A"}, {"target_uniqueness": {"value": 2, "description": "keine RR-Sätze erlaubt; Ziel eindeutig für alle RRs, deren DBRTs diese Zieleindeutigkeit und diesen Zieldatentyp haben (adreßbasierte RR's bekommen zugehörigen PTR-RR automatisch)"}, "description": "A-RR für Host", "owner_fqdn_auto_dml": {"value": 1, "description": "Automatische FQDN-Behandlung über eine Ebene"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:0100", "name": "dflt:0100,:,402,A"}, {"target_uniqueness": {"value": 0, "description": "RR-Sätze erlaubt; keine Zieleindeutigkeit verlangt"}, "description": "A-RR-Set für Domain", "owner_fqdn_auto_dml": {"value": 0, "description": "Keine automatische FQDN-Behandlung"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:1100", "name": "dflt:1100,:,400,A"}, {"target_uniqueness": {"value": 2, "description": "keine RR-Sätze erlaubt; Ziel eindeutig für alle RRs, deren DBRTs diese Zieleindeutigkeit und diesen Zieldatentyp haben (adreßbasierte RR's bekommen zugehörigen PTR-RR automatisch)"}, "description": "A-RR für Domain", "owner_fqdn_auto_dml": {"value": 0, "description": "Keine automatische FQDN-Behandlung"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:1100", "name": "dflt:1100,:,402,A"}, {"target_uniqueness": {"value": 0, "description": "RR-Sätze erlaubt; keine Zieleindeutigkeit verlangt"}, "description": "A-RR-Set für Wildcard", "owner_fqdn_auto_dml": {"value": 1, "description": "Automatische FQDN-Behandlung über eine Ebene"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "wildcard:0000", "name": "wildcard:0000,:,400,A"}].to_json)

      stub_request(:post, "http://example.org/api/2.1/dns/record/create/")
      stub_request(:get, %r{http://example.org/api/2.1/dns/record/list\?fqdn=foo.bar.org.\&inttype=dflt.*})
        .to_return(body: [
                     {fqdn: "foo.bar.org.", data: "42.42.42.42", inttype: "irrelevantfoobar"}
                   ].to_json)
      stub_request(:post, "http://example.org/api/2.1/dns/record/delete/")
    end

    ## TODO DISABLED TEST ##
    def disabled_test_subnet_enabled_by_foreman
      set_up

      assert !(backend.subnet_enabled_by_foreman? backend.defer_exact_value("42.42.24.0"), "A")

      IP.new(backend.defer_exact_value("42.42.42.0/26")).to_range.each do |ip|
        assert backend.subnet_enabled_by_foreman? ip, "A"
      end

      assert !(backend.subnet_enabled_by_foreman? backend.defer_exact_value("42.42.42.128"), "A")
    end

    ## TODO DISABLED TEST ##
    def disabled_test_subnet_enabled_by_foreman_setting
      set_up

      assert backend(dns_only_for_netdb_subnets: false).subnet_enabled_by_foreman? backend.defer_exact_value("42.42.42.42"), "A"
      assert backend(dns_only_for_netdb_subnets: false).subnet_enabled_by_foreman? backend.defer_exact_value("42.42.24.42"), "A"
      assert !(backend(dns_only_for_netdb_subnets: true).subnet_enabled_by_foreman? backend.defer_exact_value("42.42.24.42"), "A")
    end

    def test_get_netdb_subnet_by_subnet
      stub_request(:get, "http://example.org/api/2.1/dns/range/list")
        .to_return(body: "[{\"subnet_cidr_spec\": \"42.42.42.0/26\", \"description\": \"Foo Subnet\", \"ou_hierarchy_list\": [\"bar\", \"baz\"], \"name\": \"foo\", \"ou_type\": \"DE\"}]")

      h = backend

      snet = IP.new("42.42.42.0/26")

      h.connected do
        subnet = h.get_netdb_subnet snet

        assert !subnet.nil?
        assert_equal subnet[:ip], snet
        assert_equal subnet[:ip].pfxlen, 26
        assert_equal subnet[:name], "foo"
      end
    end

    def test_get_netdb_subnet_at_start
      stub_request(:get, "http://example.org/api/2.1/dns/range/list")
        .to_return(body: "[{\"subnet_cidr_spec\": \"42.42.42.0/26\", \"description\": \"Foo Subnet\", \"ou_hierarchy_list\": [\"bar\", \"baz\"], \"name\": \"foo\", \"ou_type\": \"DE\"}]")

      h = backend

      start = IP.new("42.42.42.0")

      h.connected do
        subnet = h.get_netdb_subnet start

        assert !subnet.nil?
        assert_equal subnet[:ip].to_addr, start.to_s
        assert_equal subnet[:ip].pfxlen, 26
        assert_equal subnet[:name],"foo"
      end
    end

    def test_get_netdb_subnet_included
      stub_request(:get, "http://example.org/api/2.1/dns/range/list")
        .to_return(body: "[{\"subnet_cidr_spec\": \"42.42.42.0/26\", \"description\": \"Foo Subnet\", \"ou_hierarchy_list\": [\"bar\", \"baz\"], \"name\": \"foo\", \"ou_type\": \"DE\"}]")

      h = backend

      included = IP.new("42.42.42.42")

      h.connected do
        subnet = h.get_netdb_subnet included

        assert !subnet.nil?
        assert subnet[:ip].to_range.include?(included)
      end
    end

    def test_get_netdb_subnet_nomatch
      stub_request(:get, "http://example.org/api/2.1/dns/range/list")
        .to_return(body: "[{\"subnet_cidr_spec\": \"42.42.42.0/26\", \"description\": \"Foo Subnet\", \"ou_hierarchy_list\": [\"bar\", \"baz\"], \"name\": \"foo\", \"ou_type\": \"DE\"}]")

      h = backend
      included = IP.new("42.42.24.42")

      h.connected do
        subnet = h.get_netdb_subnet included

        assert subnet.nil?
      end
    end

    def test_all_occupied_ips_empty
      stub_request(:get, %r{http://example\.org/api/2\.1/dns/record/list\?range=foo&type=A*})
        .to_return(body: [].to_json)

      h = backend
      h.connected do
        ips = h.all_occupied_ips "foo"

        assert ips.empty?
      end
    end

    def test_all_occupied_ips
      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=A")
        .to_return(body: [{fqdn: "baz.example.org", data: "42.42.42.1"},
                          {fqdn: "bar.example.org", data: "42.42.42.2"}
                         ].to_json)

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=AAAA")
        .to_return(body: [].to_json)

      h = backend
      h.connected do
        ips = h.all_occupied_ips "foo"

        assert ips.include? IP.new("42.42.42.1")
        assert ips.include? IP.new("42.42.42.2")
        assert_equal 2, ips.length
      end
    end

    def test_all_occupied_ips_nomatch
      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=A")
        .to_return(body: [{fqdn: "baz.example.org", data: "42.42.42.1"},
                          {fqdn: "bar.example.org", data: "42.42.42.2"}
                         ].to_json)

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=AAAA")
        .to_return(body: [].to_json)

      h = backend
      h.connected do
        ips = h.all_occupied_ips "foo"

        assert ips.include? IP.new("42.42.42.1")
        assert ips.include? IP.new("42.42.42.2")
        assert_equal 2, ips.length
      end
    end

    def test_rank_inttypes
      set_up

      r = backend
      r.connected do
        r.api.default.dns.do {|dns|
          inttypes = r.rank_inttypes dns, "A"

          assert_equal 4, inttypes.length
          assert_equal "dflt:0100,:,402,A", inttypes[0][:name]
          assert_equal "dflt:0100,:,400,A", inttypes[1][:name]
          assert_equal "dflt:1100,:,402,A", inttypes[2][:name]
          assert_equal "dflt:1100,:,400,A", inttypes[3][:name]
        }
      end
    end

    def test_do_create
      set_up

      # TODO: This is a Stub
      res = backend.do_create("foo.bar.org", backend.defer_exact_value("42.42.42.42"), "A")

      assert_nil res
    end

    def test_do_remove
      set_up

      # TODO: This is a Stub
      res = backend.do_remove("foo.bar.org", "A")

      assert_nil res
    end
  end
end
