require 'test_helper'
require 'webmock/test_unit'
require 'mocha/test_unit'
require 'rack/test'

require 'smart_proxy_for_testing'
require 'smart_proxy_kit_netdb/kit_netdb'
require 'smart_proxy_kit_netdb/kit_netdb_api'

module KIT
  class NetDBApiTest < Test::Unit::TestCase
    include Rack::Test::Methods

    def app
      Proxy::KIT::NetDB::Api.new
    end

    ### TODO DISABLED TEST
    def _test_unmapped_ip
      stub_request(:get, "http://example.org/api/2.1/dns/range/list")
        .to_return(body: "[{\"subnet_cidr_spec\": \"42.42.42.0/26\", \"description\": \"Foo Subnet\", \"ou_hierarchy_list\": [\"bar\", \"baz\"], \"name\": \"foo\", \"ou_type\": \"DE\"}]")

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=A")
        .to_return(body: [{fqdn: "baz.example.org", data: "42.42.42.1"},
                          {fqdn: "bar.example.org", data: "42.42.42.2"}
                         ].to_json)

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=AAAA")
        .to_return(body: [].to_json)



      get '/dns/unmapped_ip', subnet: "42.42.24.0/26"
      assert !last_response.ok?

      ips = (0..61).map do ||
        get 'dns/unmapped_ip', subnet: "42.42.42.0/26"
        puts last_response.body
        assert last_response.ok?
      end

      ips.each {|ip|
        assert IP.new("42.42.42.0/26").to_range.include? ip
        assert_not_equal ip, IP.new("42.42.42.1")
        assert_not_equal ip, IP.new("42.42.42.2")
      }

      get 'dns/unmapped_ip', subnet: "42.42.42.0"
      assert !last_response.ok?
    end
  end
end
