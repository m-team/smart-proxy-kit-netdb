require 'smart_proxy_kit_netdb/utils'

module Proxy
  module KIT
    module NetDB
      module DNS
        class ListTest < Test::Unit::TestCase
          def test_domain_list
            d = DomainList.new nil
            assert !d.include?(:unknown)
            assert !d.include?("foo.example.org.")

            assert DomainList.new("foo.example.org").include?("foo.example.org")
            assert DomainList.new("foo.example.org").include?("foo.example.org.")
            assert DomainList.new("foo.example.org.").include?("foo.example.org")
            assert DomainList.new("foo.example.org.").include?("foo.example.org.")

            assert DomainList.new(:unknown).include?(:unknown)
            assert !DomainList.new("foo.example.org").include?(:unknown)
            assert !DomainList.new(:unknown).include?("foo.example.org")
          end

          def test_subnet_list
            d = SubnetList.new nil
            assert !d.include?(:unknown)
            assert !d.include?("42.42.42.42")

            assert SubnetList.new("42.42.42.42").include?("42.42.42.42")
            assert SubnetList.new("42.42.42.42").include?(IP.new("42.42.42.42"))
            assert SubnetList.new(IP.new("42.42.42.42")).include?("42.42.42.42")
            assert SubnetList.new(IP.new("42.42.42.42")).include?(IP.new("42.42.42.42"))

            assert SubnetList.new(:unknown).include?(:unknown)
            assert !SubnetList.new("42.42.42.42").include?(:unknown)
            assert !SubnetList.new(:unknown).include?("42.42.42.42")
          end

        end
      end
    end
  end
end
