require 'deep_cover/builtin_takeover'
require 'simplecov'
SimpleCov.start

require 'test/unit'
$: << File.join(File.dirname(__FILE__), '..', 'lib')

require 'smart_proxy_for_testing'
