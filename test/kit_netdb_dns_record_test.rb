# coding: utf-8
require 'test_helper'
require 'test/unit'
require 'ip'

require 'smart_proxy_kit_netdb/kit_netdb_provider'

module KIT
  class NetDBProviderTest < Test::Unit::TestCase
    def record **kwargs
      Proxy::KIT::NetDB::DNS::Record.send :public

      Proxy::SETTINGS.foreman_url = "http://example.org"
      Proxy::KIT::NetDB::DNS::Record.new({tls: false,
                                          netdb_server: "http://example.org",
                                          dns_only_for_netdb_subnets: false}.merge(**kwargs))
    end

    def set_up
      stub_request(:get, %r{http://example.org/api/2.1/dns/record/list\?fqdn=example.org.\&type=A})
        .to_return(body: [
                     {fqdn: "example.org.", data: "42.42.42.42", inttype: "irrelevantfoobar"}
                   ].to_json)
      stub_request(:get, %r{http://example.org/api/2.1/dns/record/list\?fqdn=example.org.\&inttype=dflt.*})
        .to_return(body: [
                     {fqdn: "example.org.", data: "42.42.42.42", inttype: "irrelevantfoobar"}
                   ].to_json)

      stub_request(:get, "http://example.org/api/2.1/dns/record_inttype/list?record_type=A")
        .to_return(body: [{"target_uniqueness": {"value": 0, "description": "RR-Sätze erlaubt; keine Zieleindeutigkeit verlangt"}, "description": "A-RR-Set für Host", "owner_fqdn_auto_dml": {"value": 1, "description": "Automatische FQDN-Behandlung über eine Ebene"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:0100", "name": "dflt:0100,:,400,A"}, {"target_uniqueness": {"value": 2, "description": "keine RR-Sätze erlaubt; Ziel eindeutig für alle RRs, deren DBRTs diese Zieleindeutigkeit und diesen Zieldatentyp haben (adreßbasierte RR's bekommen zugehörigen PTR-RR automatisch)"}, "description": "A-RR für Host", "owner_fqdn_auto_dml": {"value": 1, "description": "Automatische FQDN-Behandlung über eine Ebene"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:0100", "name": "dflt:0100,:,402,A"}, {"target_uniqueness": {"value": 0, "description": "RR-Sätze erlaubt; keine Zieleindeutigkeit verlangt"}, "description": "A-RR-Set für Domain", "owner_fqdn_auto_dml": {"value": 0, "description": "Keine automatische FQDN-Behandlung"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:1100", "name": "dflt:1100,:,400,A"}, {"target_uniqueness": {"value": 2, "description": "keine RR-Sätze erlaubt; Ziel eindeutig für alle RRs, deren DBRTs diese Zieleindeutigkeit und diesen Zieldatentyp haben (adreßbasierte RR's bekommen zugehörigen PTR-RR automatisch)"}, "description": "A-RR für Domain", "owner_fqdn_auto_dml": {"value": 0, "description": "Keine automatische FQDN-Behandlung"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "dflt:1100", "name": "dflt:1100,:,402,A"}, {"target_uniqueness": {"value": 0, "description": "RR-Sätze erlaubt; keine Zieleindeutigkeit verlangt"}, "description": "A-RR-Set für Wildcard", "owner_fqdn_auto_dml": {"value": 1, "description": "Automatische FQDN-Behandlung über eine Ebene"}, "target_addr_inttype": {"value": "4", "description": "IPv4-Adresse"}, "owner_fqdn_uniqueness": {"value": 0, "description": "entsprechend Grundbedingung"}, "record_type": "A", "owner_fqdn_inttype": "wildcard:0000", "name": "wildcard:0000,:,400,A"}].to_json)
    end

    def test_dumb_nothing
      do_nothing_record = record dont_update_per_netdb: {domains: 'example.org', subnets: '42.42.42.0/24'}

      do_nothing_record.do_create 'example.org', '42.42.42.42', 'A'

      assert_raise {
        do_nothing_record.do_remove 'example.org', 'A'
      }

      set_up

      do_nothing_record.do_remove 'example.org', 'A'
    end

    def test_dumb_script
      set_up

      success_record = record dont_update_per_netdb: {domains: 'example.org', subnets: '42.42.42.0/24'},
                              update_per_script: {script: 'true', domains: 'example.org', subnets: '42.42.42.0/24'}

      failure_record = record dont_update_per_netdb: {domains: 'example.org', subnets: '42.42.42.0/24'},
                              update_per_script: {script: 'false', domains: 'example.org', subnets: '42.42.42.0/24'}

      success_record.do_create 'example.org', '42.42.42.42', 'A'
      success_record.do_remove 'example.org', 'A'

      assert_raise {
        failure_record.do_create 'example.org', '42.42.42.42', 'A'
        failure_record.do_remove 'example.org', 'A'
      }
    end

    def test_dumb_mail
      set_up

      success_record = record dont_update_per_netdb: {domains: 'example.org', subnets: '42.42.42.0/24'},
                              update_per_mail: {mailto: 'uwdkl@student.kit.edu', smtp_proxy: 'smarthost.kit.edu',
                                                domains: 'example.org', subnets: '42.42.42.0/24'}

      failure_record = record dont_update_per_netdb: {domains: 'example.org', subnets: '42.42.42.0/24'},
                              update_per_mail: {mailto: 'uwdkl@student.kit.edu', smtp_proxy: 'nosmtp.example.org',
                                                domains: 'example.org', subnets: '42.42.42.0/24'}

      success_record.do_create 'example.org', '42.42.42.42', 'A'
      success_record.do_remove 'example.org', 'A'

      assert_raise {
        failure_record.do_create 'example.org', '42.42.42.42', 'A'
        failure_record.do_remove 'example.org', 'A'
      }
    end

    def test_dumb_netdb
      set_up

      assert_raise {
        record.do_create 'example.org', '42.42.42.42', 'A'
        record.do_remove 'example.org', '42.42.42.42', 'A'
      }

      stub_request(:post, "http://example.org/api/2.1/dns/record/create/")
      stub_request(:post, "http://example.org/api/2.1/dns/record/delete/")
      record.do_create 'example.org', '42.42.42.42', 'A'
      record.do_remove 'example.org', 'A'

    end

    def test_matrix
      # TODO
    end
  end
end
