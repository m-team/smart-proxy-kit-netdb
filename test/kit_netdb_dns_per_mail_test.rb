require 'test_helper'
require 'rack/test'

require 'smart_proxy_for_testing'
require 'smart_proxy_kit_netdb/kit_netdb'
require 'smart_proxy_kit_netdb/kit_netdb_api'

module KIT
  class NetDBPerMailTest < Test::Unit::TestCase
    include Rack::Test::Methods

    DummyValueCache = Proxy::KIT::NetDB::DNS::NetDBBackedRecord::DummyValueCache

    def backend **kwargs
      Proxy::KIT::NetDB::DNS::NetDBBackedRecord.send :public
      Proxy::KIT::NetDB::DNS::MailBackedRecord.new({update_per_mail: {
                                                      smtp_proxy: 'smarthost.kit.edu',
                                                      domains: 'example.org',
                                                      subnets: '42.42.42.0/24'}.merge(kwargs)})
    end

    def test_do_create
      backend(mailto: 'uwdkl@student.kit.edu').do_create 'foo.example.org', DummyValueCache.new('42.42.42.42'), 'A'
    end

    def test_enabled_for
      assert backend(mailto: 'uwdkl@student.kit.edu').enabled_for? 'foo.example.org', DummyValueCache.new('42.42.42.42'), 'A'

      assert !(backend.enabled_for? 'foo.example.org', DummyValueCache.new('42.42.42.42'), 'A')

      assert !(backend(mailto: 'uwdkl@student.kit.edu').enabled_for? 'foo.example.de', DummyValueCache.new('42.42.42.42'), 'A')
      assert backend(mailto: 'uwdkl@student.kit.edu').enabled_for? 'foo.example.de', DummyValueCache.new('bla.blub'), 'CNAME'

      assert !(backend(mailto: 'uwdkl@student.kit.edu').enabled_for? 'foo.example.org', DummyValueCache.new('42.42.24.42'), 'A')
    end
  end
end
