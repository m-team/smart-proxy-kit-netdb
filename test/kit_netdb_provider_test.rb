require 'test_helper'
require 'test/unit'
require 'ip'

require 'smart_proxy_kit_netdb/kit_netdb_provider'

module KIT
  class NetDBProviderTest < Test::Unit::TestCase
    DummyValueCache = Proxy::KIT::NetDB::DNS::NetDBBackedRecord::DummyValueCache

    def provider **kwargs
      Proxy::KIT::NetDB::Provider.send :public, :find_free_ip, :netdb_backend, :script_backend
      Proxy::KIT::NetDB::Provider.new tls: false, netdb_server: "http://example.org", **kwargs
    end

    def test_find_free_ip_empty
      assert_raise {
        provider.find_free_ip [], []
      }
    end

    def test_find_free_ip_any
      range = IP.new("42.42.42.0/26").to_range

      p = provider
      ip = p.find_free_ip range, []

      assert range.include? ip
      assert p.instance_variable_get(:@allocated_ips).include? ip
    end

    def test_find_free_ip_with_occupied
      range = IP.new("42.42.42.0/26").to_range
      occupied = IP.new("42.42.42.32/27").to_range

      p = provider
      ip = p.find_free_ip range, occupied

      assert range.include? ip
      assert p.instance_variable_get(:@allocated_ips).include? ip
      assert !(occupied.include? ip)
    end

    def test_find_free_ips
      range = IP.new("42.42.42.0/26").to_range
      occupied = IP.new("42.42.42.32/27").to_range

      p = provider
      ips = (0..31).map {|_| p.find_free_ip range, occupied}

      ips.all? {|ip|
        assert range.include? ip
        assert !(occupied.include? ip)
      }

      assert_raise {
        # Should be full by now
        p.find_free_ip range, occupied
      }

      # When we allow the `occupied' ones, should work again
      ip = p.find_free_ip range, []
      assert range.include? ip
      assert occupied.include? ip
    end

    def test_unmapped_ip
      stub_request(:get, "http://example.org/api/2.1/dns/range/list")
        .to_return(body: "[{\"subnet_cidr_spec\": \"42.42.42.0/26\", \"description\": \"Foo Subnet\", \"ou_hierarchy_list\": [\"bar\", \"baz\"], \"name\": \"foo\", \"ou_type\": \"DE\"}]")

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=A")
        .to_return(body: [{fqdn: "baz.example.org", data: "42.42.42.1"},
                          {fqdn: "bar.example.org", data: "42.42.42.2"}
                         ].to_json)

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=AAAA")
        .to_return(body: [].to_json)


      assert_raise {
        provider.unmapped_ip subnet: "42.42.24.0/26"
      }

      p = provider
      ips = (1..61).map {|| p.unmapped_ip subnet: "42.42.42.0/26"}

      ips.each {|ip|
        assert IP.new("42.42.42.0/26").to_range.include? ip
        assert_not_equal ip, IP.new("42.42.42.1")
        assert_not_equal ip, IP.new("42.42.42.2")
      }

      assert_raise {
        p.unmapped_ip subnet: "42.42.42.0/26"
      }
    end

    def test_unmapped_ip_dualquery
      stub_request(:get, "http://example.org/api/2.1/dns/range/list")
        .to_return(body: "[{\"subnet_cidr_spec\": \"42.42.42.0/26\", \"description\": \"Foo Subnet\", \"ou_hierarchy_list\": [\"bar\", \"baz\"], \"name\": \"foo\", \"ou_type\": \"DE\"}]")

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=A")
        .to_return(body: [{fqdn: "baz.example.org", data: "42.42.42.1"},
                          {fqdn: "bar.example.org", data: "42.42.42.2"}
                         ].to_json)

      stub_request(:get, "http://example.org/api/2.1/dns/record/list?range=foo&type=AAAA")
        .to_return(body: [].to_json)

      p = provider update_per_script: {
                     script: 'echo -n + 42.42.42.42 && true',
                     domains: 'example.org.',
                     subnets: '42.42.42.0/26'
                   }

      assert_equal p.unmapped_ip(subnet: "42.42.42.0/26"), IP.new("42.42.42.42")

      p = provider update_per_script: {
                     script: 'echo 42.42.42.2',
                     domains: 'example.org.',
                     subnets: '42.42.42.0/26'
                   }

      assert_raise {
        p.unmapped_ip subnet: "42.42.42.0/26"
      }
    end

    def test_example_script
      p = provider update_per_script: {
                     script: 'scripts/update/example.sh',
                     domains: 'example.org.',
                     subnets: '42.42.42.0/24'
                   },
                   dont_update_per_netdb: {
                     domains: 'example.org.',
                     subnets: '42.42.42.0/24'
                   }

      assert !(p.netdb_backend.enabled_for? "foo.example.org", DummyValueCache.new("42.42.42.42"), "A")
      assert p.script_backend.enabled_for? "foo.example.org", DummyValueCache.new("42.42.42.42"), "A"

      for i in 1..127 do
        p.script_backend.do_create "foo#{i}.example.org", DummyValueCache.new("42.42.42.#{i}"), 'A'
      end

      for i in 128..256 do
        ip = p.unmapped_ip subnet: "42.42.42.0/24"
        for i in 1..127 do
          assert_not_equal ip, IP.new("42.42.42.#{i}")
        end
      end

      assert_raise {
        p.unmapped_ip subnet: "42.42.42.0/24"
      }

      for i in 1..127 do
        p.script_backend.do_remove "foo#{i}.example.org", 'A'
        assert_equal p.unmapped_ip(subnet: "42.42.42.0/24"), IP.new("42.42.42.#{i}")
      end
    end
  end
end
