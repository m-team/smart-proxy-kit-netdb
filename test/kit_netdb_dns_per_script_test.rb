require 'test_helper'
require 'rack/test'

require 'smart_proxy_for_testing'
require 'smart_proxy_kit_netdb/kit_netdb'
require 'smart_proxy_kit_netdb/kit_netdb_api'

module KIT
  class NetDBPerScriptTest < Test::Unit::TestCase
    include Rack::Test::Methods

    DummyValueCache = Proxy::KIT::NetDB::DNS::NetDBBackedRecord::DummyValueCache

    def backend **kwargs
      Proxy::KIT::NetDB::DNS::ScriptBackedRecord.send :public
      Proxy::KIT::NetDB::DNS::ScriptBackedRecord.new({update_per_script: {
                                                      domains: 'example.org',
                                                      subnets: '42.42.42.0/24'}.merge(kwargs)})
    end

    def test_do_create
      backend(script: 'true').do_create 'foo.example.org', DummyValueCache.new('42.42.42.42'), 'A'
      backend(script: 'echo').do_create 'foo.example.org.', DummyValueCache.new('42.42.42.42'), 'A'

      assert_raise {
        backend(script: 'false').do_create 'foo.example.org', DummyValueCache.new('42.42.42.42'), 'A'
      }
    end

    def test_do_remove
      backend(script: 'true').do_remove 'foo.example.org', 'A'
      backend(script: 'echo').do_remove 'foo.example.org.', 'A'

      assert_raise {
        backend(script: 'false').do_remove 'foo.example.org', 'A'
      }
    end

    def test_get_free
      backend(script: 'true').get_free '42.42.42.0/24'

      assert_raise {
        backend(script: 'false').get_free '42.42.42.0/24'
      }
    end

    def test_run_update_script
      lines = backend(script: 'echo').run_update_script :create, 'fqdn', 'value', 'TYPE'
      assert_equal lines, ["--create fqdn value TYPE"]

      lines = backend(script: 'echo').run_update_script :remove, 'fqdn', 'TYPE'
      assert_equal lines, ["--remove fqdn TYPE"]

      lines = backend(script: 'echo').run_update_script :suggest, 'sub/net'
      assert_equal lines, ["--suggest sub/net"]

      assert_raise {
        backend(script: 'ls').run_update_script :remove, 'fqdn', 'TYPE'
      }
    end

    def test_enabled_for
      assert backend(script: 'true').enabled_for? 'foo.example.org', DummyValueCache.new('42.42.42.42'), 'A'
      assert backend(script: 'true').enabled_for? nil, DummyValueCache.new('42.42.42.42'), 'A'

      assert !(backend.enabled_for? 'foo.example.org', DummyValueCache.new('42.42.42.42'), 'A')

      assert !(backend(script: 'true').enabled_for? 'foo.example.de', DummyValueCache.new('42.42.42.42'), 'A')
      assert backend(script: 'true').enabled_for? 'foo.example.de', DummyValueCache.new('bla.blub'), 'CNAME'

      assert !(backend(script: 'true').enabled_for? 'foo.example.org', DummyValueCache.new('42.42.24.42'), 'A')
    end
  end
end
