require File.expand_path('../lib/smart_proxy_kit_netdb/version', __FILE__)

Gem::Specification.new do |s|
  s.name = 'smart_proxy_kit_netdb'
  s.version = Proxy::KIT::NetDB::VERSION
  s.authors = ['Joshua Bachmeier']
  s.email = 'uwdkl@student_kit.edu'

  s.summary = 'KIT NetDB smart proxy plugin'
  s.homepage = 'https://git.scc.kit.edu/m-team/smart-proxy-kit-netdb'

  s.files = Dir['{scripts,lib,settings.d,bundler.d}/**/*'] + ['README.md']
  s.post_install_message = ("If not done so already, don't forget to add this gem to foreman-proxy:\n" +
                            " $ echo \"gem 'smart_proxy_kit_netdb'\" > ~foreman-proxy/bundler.d/kit_netdb.rb")

  s.add_dependency 'kit-netdb'
  s.add_dependency 'ruby-ip'

  s.add_development_dependency 'rake'
  s.add_development_dependency 'rack-test'
  s.add_development_dependency 'test-unit'
  s.add_development_dependency 'webmock'
  s.add_development_dependency 'mocha'
  s.add_development_dependency 'smart_proxy'
  s.add_development_dependency 'byebug'
  s.add_development_dependency 'irb'
end
