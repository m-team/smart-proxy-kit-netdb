# Smart Proxy - KIT::NetDB

This plug-in adds support for [KIT NetDB](https://www-net-doku.scc.kit.edu/webapi/2.0/intro) to Foreman's Smart Proxy.

This is the other side of [m-team/foreman-kit-netdb](https://git.scc.kit.edu/m-team/foreman-kit-netdb)

## Usage

Refer to the [wiki](https://git.scc.kit.edu/m-team/smart-proxy-kit-netdb/wikis/home).

## Contributing

Fork and send a Pull Request. Thanks!
